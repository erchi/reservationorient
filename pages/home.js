import React, {Component} from 'react'
import Auth from '../lib/Auth';
import Nav from "../components/nav";
const auth = new Auth();

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            user: {}
        }
    }

    componentDidMount(){
        var user_data = localStorage.getItem('user_details');
        var isLoggedIn = localStorage.getItem('isLoggedIn');
        this.setState({
            user : JSON.parse(user_data)
        });
        if(!isLoggedIn || !user_data){
            window.location.replace('/')
        }
    }

    logout(){
        auth.logout()
    }

    render(){

        return(
            <div>
                <Nav />
                <h1>
                    Your account
                </h1>
                {console.log(this.state.user)}
                <h2 className='bg-red'>{this.state.user.nickname}</h2>
                <h2>{this.state.user.email}</h2>
                <img src={this.state.user.picture} alt=""/>


                <button onClick={()=>this.logout()}>
                    Logout
                </button>
            </div>
        )
    }
}