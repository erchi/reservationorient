import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'

const Dashboard = () => (
    <div>
        <Head>
            <title>Dashboard</title>
        </Head>

        <Nav />

        <div>
            <h1>Dashboard</h1>
        </div>
    </div>
)

export default Dashboard
