import React from 'react'
import Link from 'next/link'
import Auth from '../lib/Auth';
const auth = new Auth();

export default class Nav extends React.Component {


    handleLogin(){
        auth.login()
    }

    render() {
        return (
            <div>
            <Link href="/">
                <a>Home</a>
            </Link>

            <Link href="/dashboard">
                 <a>Dashboard</a>
            </Link>
                <button onClick={()=>this.handleLogin()}>Login | Register</button>
            </div>
        )
    }
}