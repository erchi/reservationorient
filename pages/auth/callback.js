import React, { Component } from 'react'
import Auth from '../../lib/Auth';
import Router from 'next/router'

const auth = new Auth();

export default class Callback extends Component {
    componentDidMount(){
        var user_details = auth.extractInfoFromHash();
        auth.handleAuthentication().then((res)=>{
            if(!res){
                window.location.replace('/')
            }
            else{
                Router.push('/home');
            }
        })
    }

    render() {
        return (
            <div>
                <h1>
                    Loading...
                </h1>
            </div>
        )
    }
}