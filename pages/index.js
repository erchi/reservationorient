import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'
import '../css/styles.css';

const Home = () => (
  <div>
    <Head>
      <title>Home</title>
    </Head>

    <Nav />

    <div>
      <h1 className='p-4 bg-red-300'>Welcome Home</h1>
    </div>
  </div>
)

export default Home
